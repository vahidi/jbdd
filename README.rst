****************************************
JBDD, a Java interface to CUDD and BuDDY
****************************************

JBDD is a Java interface (JNI) for the two popular BDD libraries BuDDy and CUDD (and SBL, but that one is very unofficial).
This allows you to use your favourite programming language [Java] and still have the benefits of using the current state of the art BDD packages. 

If you need a pure java BDD package, check out my `JDD <https://bitbucket.org/vahidi/jdd>`_ Java BDD package instead. 


BDD?
****

`Binary Decision Diagrams <https://en.wikipedia.org/wiki/Binary_decision_diagram>`_
(BDDs) are used in CSP, formal verification and optimisation.
To work with BDDs, you need a BDD package, which is basically the BDD software library.
CUDD and BuDDy are two state-of-the-art BDD packages written in C and C++. 

Java?
*****
It is no secret that Java is more productive than C and C++. 
The performance impact is nowadays acceptable, so why not use Java for your research project?

